# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [2022-05-16]

### Removed
- Log Shipper as it has been superceeded by the logging agent

## [2022-05-08]

### Added
- Constraint to service nodes

### Changed
- Volume templates to use axion-proxima-smb provider

## [2022-05-07]

### Added
- Moved to new repo group

### Changed
- Made volume definitions use templates

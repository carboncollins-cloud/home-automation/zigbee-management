# Home Automation - Phoscon & deCONZ

[[_TOC]]

## Description

[Phoscon & deCONZ](https://phoscon.de/en/conbee2/software) instances for connecting to and managing a ZigBee network for home automation.

## Hardware

This is intended to be used with the [ConBee II](https://phoscon.de/en/conbee2/) USB Stick which is mounted as a device into the jobs docker image.

The USB device is discovered and mounted in Nomad making use of the [USB Device Plugin](https://gitlab.com/CarbonCollins/nomad-usb-device-plugin)
